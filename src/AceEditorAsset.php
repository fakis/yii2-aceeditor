<?php

namespace fakis\aceeditor;

use yii\web\AssetBundle;

/**
 * Class AceEditorAsset
 * @package fakis\aceeditor
 *
 * @author Fakis <fakis738@qq.com>
 */
class AceEditorAsset extends AssetBundle
{
    /**
     * @inheritdoc
     */
    public $sourcePath = '@fakis/aceeditor/assets';

    /**
     * @inheritdoc
     */
    public $js = [
        'ace.js'
    ];

    public $depends = [
        'yii\web\YiiAsset'
    ];
}