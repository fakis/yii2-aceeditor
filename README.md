Yii2 AceEditor
=========
> 在线代码编辑器，针对Yii2框架封装的Ace编辑器

构建于AceEditor最新版本v1.4.14
- [Github](https://github.com/ajaxorg/ace-builds)
- [Npm](https://www.npmjs.com/package/ace-builds)

安装
---

通过 [composer](http://getcomposer.org/download/) 安装此扩展

```
php composer.phar require --prefer-dist fakis/yii2-aceeditor "*"
```

或者在`composer.json`文件的require部分填上

```
"fakis/yii2-aceeditor": "*"
```

使用
---
单独使用小部件

```php
\fakis\aceeditor\AceEditor::widget([
	// 您可以将它用于模型属性
	'model' => $model,
	'attribute' => 'field',

	// 或者仅用于字段
	'name' => 'input_name',

    'mode' => 'html', // 代码语言模式，默认："html"
    'theme' => 'monokai', // 编辑器主题，默认："monokai"
    'fontSize' => 16, // 编辑器字体大小，默认："14"
    'readOnly' => 'true', // 是否只读，默认："false"
]);
```

用于表单字段小部件

```php
$form = \yii\widgets\ActiveForm::begin();

echo $form->field($model, 'field')->widget('\fakis\aceeditor\AceEditor', [
    'mode'=>'html', // 代码语言模式，默认："html"
    'theme'=>'monokai', // 编辑器主题，默认："monokai"
    'fontSize' => 16, // 编辑器字体大小，默认："14"
    'readOnly' => 'true', // 是否只读，默认："false"
]);

\yii\widgets\ActiveForm::end();
```